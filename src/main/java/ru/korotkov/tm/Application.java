package ru.korotkov.tm;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.dao.ProjectDAO;
import ru.korotkov.tm.dao.TaskDAO;
import ru.korotkov.tm.entity.Project;
import ru.korotkov.tm.entity.Task;

import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Application {

    private final ProjectDAO projectDAO = new ProjectDAO();

    private final TaskDAO taskDAO = new TaskDAO();

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    {
        projectDAO.create("1", "2");
        taskDAO.create("3", "4");
    }

    public static void main(String[] args) {
        Application app = new Application();
        app.displayWelcome();
        app.run();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        String command;
        while (scanner.hasNextLine()) {
            command = scanner.nextLine();
            if (!process(command)) {
                break;
            }
        }
    }

    public boolean process(String line) {
        if (line == null || line.isEmpty()) {
            return true;
        }

        if (TerminalConst.CMD_EXIT.equals(line)) {
            return false;
        }

        processCommand(line);
        return true;
    }

    private void processCommand(final String line) {
        final String parts[] = line.split(TerminalConst.SPLIT);
        final String command = parts[0];
        final String[] arguments = Arrays.copyOfRange(parts, 1, parts.length);
        switch (command) {
            case TerminalConst.CMD_VERSION:
                System.out.println(bundle.getString("version"));
                break;
            case TerminalConst.CMD_ABOUT:
                System.out.println(bundle.getString("about"));
                break;
            case TerminalConst.CMD_HELP:
                System.out.println(bundle.getString("help"));
                break;
            case TerminalConst.PROJECT_CREATE:
                createProject(arguments);
                break;
            case TerminalConst.PROJECT_CLEAR:
                clearProject();
                break;
            case TerminalConst.PROJECT_LIST:
                listProject();
                break;
            case TerminalConst.PROJECT_VIEW:
                viewProject(arguments);
                break;
            case TerminalConst.PROJECT_REMOVE:
                removeProject(arguments);
                break;
            case TerminalConst.PROJECT_UPDATE:
                updateProject(arguments);
                break;
            case TerminalConst.TASK_CREATE:
                createTask(arguments);
                break;
            case TerminalConst.TASK_CLEAR:
                clearTask();
                break;
            case TerminalConst.TASK_LIST:
                listTask();
                break;
            case TerminalConst.TASK_VIEW:
                viewTask(arguments);
                break;
            case TerminalConst.TASK_REMOVE:
                removeTask(arguments);
                break;
            case TerminalConst.TASK_UPDATE:
                updateTask(arguments);
                break;
            default:
                System.out.println(String.format(bundle.getString("stub"), line));
                break;
        }
    }

    public void displayWelcome() {
        System.out.println(bundle.getString("welcome"));
    }

    private void createProject(final String[] arguments) {
        final String name = arguments.length > 0 ? arguments[0] : "";
        final String description = arguments.length > 1 ? arguments[1] : "";
        projectDAO.create(name, description);
        System.out.println(bundle.getString("projectCreate"));
    }

    private void clearProject() {
        projectDAO.clear();
        System.out.println(bundle.getString("projectClear"));
    }

    private void viewProject(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectDAO.findByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_NAME:
                project = projectDAO.findByName(param);
                break;
            case TerminalConst.OPTION_ID:
                project = projectDAO.findById(Long.parseLong(param));
                break;
        }
        displayProject(project);
    }

    private void displayProject(Project project) {
        if (project == null) {
            System.out.println(bundle.getString("notFound"));
            return;
        }

        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    private void removeProject(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectDAO.removeByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_NAME:
                project = projectDAO.removeByName(param);
                break;
            case TerminalConst.OPTION_ID:
                project = projectDAO.removeById(Long.parseLong(param));
                break;
        }
        displayProject(project);
    }

    private void updateProject(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        final String name = arguments.length > 2 ? arguments[2] : null;
        final String description = arguments.length > 3 ? arguments[3] : null;
        if (option == null || param == null || name == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectDAO.updateByIndex(Integer.parseInt(param) - 1, name, description);
                break;
            case TerminalConst.OPTION_ID:
                project = projectDAO.updateById(Long.parseLong(param), name, description);
                break;
        }
        displayProject(project);
    }

    private void listProject() {
        int index = 0;
        for (final Project project : projectDAO.findAll()) {
            System.out.println("INDEX: " + index++ + " ID: " + project.getId() + " " + project.getName() + ": " + project.getDescription());
        }
    }

    private void createTask(final String[] arguments) {
        final String name = arguments.length > 0 ? arguments[0] : "";
        final String description = arguments.length > 1 ? arguments[1] : "";
        taskDAO.create(name, description);
        System.out.println(bundle.getString("taskCreate"));
    }

    private void clearTask() {
        taskDAO.clear();
        System.out.println(bundle.getString("taskClear"));
    }

    private void viewTask(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskDAO.findByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_NAME:
                task = taskDAO.findByName(param);
                break;
            case TerminalConst.OPTION_ID:
                task = taskDAO.findById(Long.parseLong(param));
                break;
        }
        displayTask(task);
    }

    private void displayTask(Task task) {
        if (task == null) {
            System.out.println(bundle.getString("notFound"));
            return;
        }

        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    private void removeTask(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskDAO.removeByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_NAME:
                task = taskDAO.removeByName(param);
                break;
            case TerminalConst.OPTION_ID:
                task = taskDAO.removeById(Long.parseLong(param));
                break;
        }
        displayTask(task);
    }

    private void updateTask(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        final String name = arguments.length > 2 ? arguments[2] : null;
        final String description = arguments.length > 3 ? arguments[3] : null;
        if (option == null || param == null || name == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskDAO.updateByIndex(Integer.parseInt(param) - 1, name, description);
                break;
            case TerminalConst.OPTION_ID:
                task = taskDAO.updateById(Long.parseLong(param), name, description);
                break;
        }
        displayTask(task);
    }

    private void listTask() {
        int index = 0;
        for (final Task task : taskDAO.findAll()) {
            System.out.println("INDEX: " + index++ + " ID: " + task.getId() + " " + task.getName() + ": " + task.getDescription());
        }
    }

}