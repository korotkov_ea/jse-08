package ru.korotkov.tm.dao;

import ru.korotkov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectDAO {

    private List<Project> projects = new ArrayList<>();

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public List<Project> findAll() {
        return new ArrayList<>(projects);
    }

    ///////////////////////

    public Project findByIndex(final int index) {
        if (index < 0 || index >= projects.size()) {
            return null;
        }
        return projects.get(index);
    }

    public Project findByName(final String name) {
        if (name == null) {
            return null;
        }
        for (final Project project : projects) {
            if (name.equals(project.getName())) {
                return project;
            }
        }
        return null;
    }

    public Project findById(final Long id) {
        if (id == null) {
            return null;
        }
        for (final Project project : projects) {
            if (id.equals(project.getId())) {
                return project;
            }
        }
        return null;
    }

    public Project removeByIndex(final int index) {
        Project project = findByIndex(index);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    public Project removeByName(final String name) {
        Project project = findByName(name);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    public Project removeById(final Long id) {
        Project project = findById(id);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    public Project updateByIndex(final int index, final String name, final String description) {
        Project project = findByIndex(index);
        if (project == null) {
            return null;
        }
        project.setName(name);
        if (description != null) {
            project.setDescription(description);
        }
        return project;
    }

    public Project updateById(final Long id, final String name, final String description) {
        Project project = findById(id);
        if (project == null) {
            return null;
        }
        project.setName(name);
        if (description != null) {
            project.setDescription(description);
        }
        return project;
    }

}
